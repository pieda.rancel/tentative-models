const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
	email: {
		type: String,
		required:[true, "email is required"]
	},
	password:{
		type:String,
		required:[true, "password is required"]
	},
	balance: {
		type: Number,
		default: 0
	},
	records: [
		{
			recordId: {
				type:String,
				required:[true, "Record id is required"]
			},
			createdOn: {
				type: Date,
				default: new Date()
			} 
			
		}
	]
})

module.exports = mongoose.model('User', userSchema)