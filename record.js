const mongoose = require('mongoose')
const Schema = mongoose.Schema

const recordSchema = new Schema({
	categoryId: {
		type: String,
		required: [true, "Category ID is required"] 
	},
	amount:{ 
		type: Number,
		default:0
	},
	previousBalance: {
		type: Number,
		required: [true, 'Previous Balance is required']
	},
	description:{
		type: String
	}, 
	createdOn :{
		type:Date,
		default:new Date()
	} 
})

module.exports = mongoose.model('Record', recordSchema)