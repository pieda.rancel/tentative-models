const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categorySchema = new Schema({
	name:{ 
		type: String,
		required:[true, "Category Name is required"]
	},
	isActive:{ 
		type:Boolean,
		default:true
	},
	categoryType:{ 
		type:String,
		required:[true, "Category type is required"]
	},
	users: 
	[
		{		
			userId: {
				type: String,
				required: [true, "User ID is required"]
		}
	], 
	records:
	[
		{
			recordId:{
				type:String,
				required:[true, "Record id is required"]
			},
			createdOn:{
				type:Date,
				default:new Date()
			} 

		}
	]
})

module.exports = mongoose.model('Category', categorySchema)